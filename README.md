## Tic-Tac-Toe-Engine

Play a quick round of Tic-Tac-Toe against the AI by running `python demo.py`

The code is written in python 3.7 and does only need the math and random modules.

Thanks to the generic implementation, the AI can play not only Tic-Tac-Toe, but also other games. In `demo.py` uncomment some lines to play two other exemplary game.
as well.